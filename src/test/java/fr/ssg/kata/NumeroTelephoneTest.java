package fr.ssg.kata;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Kata.formaterNumeroTelephone(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) // => returns "(123) 456-7890"
 */
public class NumeroTelephoneTest {

    @Test
    public void formaterNumeroTelephone_devrait_retourner_01_00_00_00_00_quand_0100000000(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.formaterNumeroTelephone(new int[]{0,1,0,0,0,0,0,0,0,0});
        Assertions.assertEquals("01 00 00 00 00", numeroFormate);
    }

    @Test
    public void formaterNumeroTelephone_devrait_retourner_01_23_45_67_89_quand_0123456789(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.formaterNumeroTelephone(new int[]{0,1,2,3,4,5,6,7,8,9});
        Assertions.assertEquals("01 23 45 67 89", numeroFormate);
    }

    @Test
    public void formaterNumeroTelephone_devrait_retourner_Format_Errone_quand_012345678(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.formaterNumeroTelephone(new int[]{0,1,2,3,4,5,6,7,8});
        Assertions.assertEquals("Format errone", numeroFormate);
    }

    @Test
    public void createPhoneNumber_devrait_retourner_010_000_0000_quand_0100000000(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.createPhoneNumber(new int[]{0,1,0,0,0,0,0,0,0,0});
        Assertions.assertEquals("(010) 000-0000", numeroFormate);
    }

    @Test
    public void createPhoneNumber_devrait_retourner_012_345_6789_quand_0123456789(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.createPhoneNumber(new int[]{0,1,2,3,4,5,6,7,8,9});
        Assertions.assertEquals("(012) 345-6789", numeroFormate);
    }

    @Test
    public void createPhoneNumber_devrait_retourner_Bad_Format_quand_012345678(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.createPhoneNumber(new int[]{0,1,2,3,4,5,6,7,8});
        Assertions.assertEquals("Bad format", numeroFormate);        
    }


    @Test
    public void formaterNumeroTelephoneOptimise_devrait_retourner_01_00_00_00_00_quand_0100000000(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.formaterNumeroTelephoneOptimise(new int[]{0,1,0,0,0,0,0,0,0,0});
        Assertions.assertEquals("01 00 00 00 00", numeroFormate);        
    }

    @Test
    public void formaterNumeroTelephoneOptimise_devrait_retourner_01_23_45_67_89_quand_0123456789(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.formaterNumeroTelephoneOptimise(new int[]{0,1,2,3,4,5,6,7,8,9});
        Assertions.assertEquals("01 23 45 67 89", numeroFormate);        
    }

    @Test
    public void createPhoneNumberOptimized_devrait_retourner_010_000_0000_quand_0100000000(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.createPhoneNumberOptimized(new int[]{0,1,0,0,0,0,0,0,0,0});
        Assertions.assertEquals("(010) 000-0000", numeroFormate);        
    }

    @Test
    public void createPhoneNumberOptimized_devrait_retourner_012_345_6789_quand_0123456789(){
        NumeroTelephone numeroTelephone = new NumeroTelephone();
        String numeroFormate = numeroTelephone.createPhoneNumberOptimized(new int[]{0,1,2,3,4,5,6,7,8,9});
        Assertions.assertEquals("(012) 345-6789", numeroFormate);        
    }

}
