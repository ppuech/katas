package fr.ssg.kata;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Write a function that will return the count of distinct case-insensitive alphabetic characters and numeric digits
 * that occur more than once in the input string.
 *
 * The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.
 */
public class CountingDuplicateTest {

    @Test
    public void count_devrait_retourner_0_quand_0() {
        // When
        int numberDuplicate = CountingDuplicate.count("0");
        // Then
        Assertions.assertEquals(0, numberDuplicate);
    }

    @Test
    public void count_devrait_retourner_1_quand_00() {
        // When
        int numberDuplicate = CountingDuplicate.count("00");
        // Then
        Assertions.assertEquals(1, numberDuplicate);
    }

    @Test
    public void count_devrait_retourner_0_quand_a() {
        // When
        int numberDuplicate = CountingDuplicate.count("a");
        // Then
        Assertions.assertEquals(0, numberDuplicate);
    }

    @Test
    public void count_devrait_retourner_3_quand_aabbcc() {
        // When
        int numberDuplicate = CountingDuplicate.count("aabbcc");
        // Then
        Assertions.assertEquals(3, numberDuplicate);
    }

    @Test
    public void count_devrait_retourner_1_quand_aA() {
        // When
        int numberDuplicate = CountingDuplicate.count("aA");
        // Then
        Assertions.assertEquals(1, numberDuplicate);
    }

    @Test
    public void count_devrait_retourner_26_quand_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZReturnsTwentySix() {
        // When
        int numberDuplicate = CountingDuplicate.count("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZReturnsTwentySix");
        // Then
        Assertions.assertEquals(26, numberDuplicate);
    }

}
