package fr.ssg.kata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The goal of this exercise is to convert a string to a new string where each character in the new string
 * is "(" if that character appears only once in the original string,
 * or ")" if that character appears more than once in the original string.
 *
 * Ignore capitalization when determining if a character is a duplicate.
 */
public class DuplicateEncoderTest {

    // Given
    DuplicateEncoder duplicateEncoder = new DuplicateEncoder();

    @Test
    public void encode_devrait_renvoyer_string_vide_quand_string_vide(){
        // When
        String resultat = duplicateEncoder.encode("");
        // Then
        assert resultat.equals("");
    }

    @Test
    public void encode_devrait_renvoyer_parenthese_ouvrante_seule_quand_a(){

        // When
        String resultat = duplicateEncoder.encode("a");
        // Then
        assert resultat.equals("(");
    }

    @Test
    public void encode_devrait_convertir_parentheses_ouvrantes_et_fermantes_pour_t_quand_contient_une_parenthese_en_entree(){
        // When
        String resultat = duplicateEncoder.encode("test");
        // Then
        assert resultat.equals(")(()");
    }

    @Test
    public void encode_devrait_convertir_multiparentheses_quand_contient_plus_dune_occurence() {
        assertEquals(")()())()(()()(",
                duplicateEncoder.encode("Prespecialized"));

        assertEquals("))))())))",
                duplicateEncoder.encode("   ()(   "));
    }
}
