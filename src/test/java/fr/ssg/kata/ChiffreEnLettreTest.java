package fr.ssg.kata;

import org.junit.jupiter.api.Test;

public class ChiffreEnLettreTest {

    @Test
    public void convertion_devrait_renvoyer_un_quand_1() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1);
        //THEN
        assert(resultat).equals("un");
    }

    @Test
    public void convertion_devrait_renvoyer_deux_quand_2() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(2);
        //THEN
        assert(resultat).equals("deux");
    }

    @Test
    public void convertion_devrait_renvoyer_trois_quand_3() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(3);
        //THEN
        assert(resultat).equals("trois");
    }

    @Test
    public void convertion_devrait_renvoyer_dix_quand_10() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(10);
        //THEN
        assert(resultat).equals("dix");
    }

    @Test
    public void convertion_devrait_renvoyer_onze_quand_11() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(11);
        //THEN
        assert(resultat).equals("onze");
    }

    @Test
    public void convertion_devrait_renvoyer_dix_sept_quand_17() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(17);
        //THEN
        assert(resultat).equals("dix-sept");
    }

    @Test
    public void convertion_devrait_renvoyer_dix_huit_quand_18() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(18);
        //THEN
        assert(resultat).equals("dix-huit");
    }

    @Test
    public void convertion_devrait_renvoyer_vingt_quand_20() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(20);
        //THEN
        assert(resultat).equals("vingt");
    }

    @Test
    public void convertion_devrait_renvoyer_vingt_et_un_quand_21() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(21);
        //THEN
        assert(resultat).equals("vingt et un");
    }

    @Test
    public void convertion_devrait_renvoyer_vingt_deux_quand_22() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(22);
        //THEN
        assert(resultat).equals("vingt-deux");
    }

    @Test
    public void convertion_devrait_renvoyer_vingt_trois_quand_23() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(23);
        //THEN
        assert(resultat).equals("vingt-trois");
    }

    @Test
    public void convertion_devrait_renvoyer_trente_quand_30() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(30);
        //THEN
        assert(resultat).equals("trente");
    }

    @Test
    public void convertion_devrait_renvoyer_trente_et_un_quand_31() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(31);
        //THEN
        assert(resultat).equals("trente et un");
    }

    @Test
    public void convertion_devrait_renvoyer_trente_deux_quand_32() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(32);
        //THEN
        assert(resultat).equals("trente-deux");
    }

    @Test
    public void convertion_devrait_renvoyer_trente_trois_quand_33() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(33);
        //THEN
        assert(resultat).equals("trente-trois");
    }

    @Test
    public void convertion_devrait_renvoyer_soixante_dix_quand_70() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(70);
        //THEN
        assert(resultat).equals("soixante-dix");
    }

    @Test
    public void convertion_devrait_renvoyer_soixante_et_onze_quand_71() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(71);
        //THEN
        assert(resultat).equals("soixante et onze");
    }

    @Test
    public void convertion_devrait_renvoyer_soixante_douze_quand_72() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(72);
        //THEN
        assert(resultat).equals("soixante-douze");
    }

    @Test
    public void convertion_devrait_renvoyer_soixante_dix_sept_quand_77() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(77);
        //THEN
        assert(resultat).equals("soixante-dix-sept");
    }

    @Test
    public void convertion_devrait_renvoyer_quatre_vingts_quand_80() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(80);
        //THEN
        assert(resultat).equals("quatre-vingts");
    }

    @Test
    public void convertion_devrait_renvoyer_quatre_vingts_un_quand_81() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(81);
        //THEN
        assert(resultat).equals("quatre-vingts-un");
    }

    @Test
    public void convertion_devrait_renvoyer_quatre_vingts_dix_quand_90() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(90);
        //THEN
        assert(resultat).equals("quatre-vingts-dix");
    }

    @Test
    public void convertion_devrait_renvoyer_quatre_vingts_onze_quand_91() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(91);
        //THEN
        assert(resultat).equals("quatre-vingts-onze");
    }

    @Test
    public void convertion_devrait_renvoyer_quatre_vingts_dix_sept_quand_97() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(97);
        //THEN
        assert(resultat).equals("quatre-vingts-dix-sept");
    }

    @Test
    public void convertion_devrait_renvoyer_moins_quatre_vingts_seize_quand_moins_96() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(-96);
        //THEN
        assert(resultat).equals("moins quatre-vingts-seize");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_quand_100() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(100);
        //THEN
        assert(resultat).equals("cent");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_un_quand_101() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(101);
        //THEN
        assert(resultat).equals("cent un");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_vingt_et_un_quand_121() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(121);
        //THEN
        assert(resultat).equals("cent vingt et un");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_vingt_deux_quand_122() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(122);
        //THEN
        assert(resultat).equals("cent vingt-deux");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_vingt_trois_quand_123() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(123);
        //THEN
        assert(resultat).equals("cent vingt-trois");
    }

    @Test
    public void convertion_devrait_renvoyer_deux_cent_soixante_et_onze_quand_271() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(271);
        //THEN
        assert(resultat).equals("deux cents soixante et onze");
    }

    @Test
    public void convertion_devrait_renvoyer_cinq_cent_soixante_neuf_quand_569() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(569);
        //THEN
        assert(resultat).equals("cinq cents soixante-neuf");
    }

    @Test
    public void convertion_devrait_renvoyer_huit_cent_soixante_quatorze_quand_874() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(874);
        //THEN
        assert(resultat).equals("huit cents soixante-quatorze");
    }

    @Test
    public void convertion_devrait_renvoyer_huit_cent_soixante_quatorze_quand_999() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(999);
        //THEN
        assert(resultat).equals("neuf cents quatre-vingts-dix-neuf");
    }

    @Test
    public void convertion_devrait_renvoyer_mille_quand_1000() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1000);
        //THEN
        assert(resultat).equals("mille");
    }

    @Test
    public void convertion_devrait_renvoyer_mille_cinq_quand_1005() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1005);
        //THEN
        assert(resultat).equals("mille cinq");
    }

    @Test
    public void convertion_devrait_renvoyer_mille_seize_quand_1016() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1016);
        //THEN
        assert(resultat).equals("mille seize");
    }

    @Test
    public void convertion_devrait_renvoyer_mille_hui_cent_dix_neuf_quand_1819() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1819);
        //THEN
        assert(resultat).equals("mille huit cents dix-neuf");
    }

    @Test
    public void convertion_devrait_renvoyer_dix_mille_quand_10000() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(10000);
        //THEN
        assert(resultat).equals("dix mille");
    }

    @Test
    public void convertion_devrait_renvoyer_dix_mille_quand_125635() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(125635);
        //THEN
        assert(resultat).equals("cent vingt-cinq mille six cents trente-cinq");
    }

    @Test
    public void convertion_devrait_renvoyer_cent_mille_quand_100000() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(100000);
        //THEN
        assert(resultat).equals("cent mille");
    }

    @Test
    public void convertion_devrait_renvoyer_neuf_cents_quatre_vingts_dix_neuf_mille_neuf_cents_quatre_vingts_dix_neuf_quand_999999() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(999999);
        //THEN
        assert(resultat).equals("neuf cents quatre-vingts-dix-neuf mille neuf cents quatre-vingts-dix-neuf");
    }

    @Test
    public void convertion_devrait_renvoyer_un_million_quand_1000000() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1000000);
        //THEN
        assert(resultat).equals("un million");
    }

    @Test
    public void convertion_devrait_renvoyer_neuf_cents_quatre_vingts_dix_neuf_mille_millions_neuf_cents_quatre_vingts_dix_neuf_mille_neuf_cents_quatre_vingts_dix_neuf_quand_999999() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(999999999);
        //THEN
        assert(resultat).equals("neuf cents quatre-vingts-dix-neuf millions neuf cents quatre-vingts-dix-neuf mille neuf cents quatre-vingts-dix-neuf");
    }

    @Test
    public void convertion_devrait_renvoyer_un_milliard_quand_1000000000() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1000000000);
        //THEN
        assert(resultat).equals("un milliard");
    }

    @Test
    public void convertion_devrait_renvoyer_un_milliard_quand_1000000001() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(1000000001);
        //THEN
        assert(resultat).equals("un milliard un");
    }

    @Test
    public void convertion_devrait_renvoyer_neuf_cents_quatre_vingts_dix_neuf_mille_milliards_neuf_cents_quatre_vingts_dix_neuf_mille_millions_neuf_cents_quatre_vingts_dix_neuf_mille_neuf_cents_quatre_vingts_dix_neuf_quand_999999() {
        // GIVEN
        ChiffreEnLettre chiffreEnLettre = new ChiffreEnLettre();
        // WHEN
        String resultat = chiffreEnLettre.convertion(999999999999L);
        //THEN
        assert(resultat).equals("neuf cents quatre-vingts-dix-neuf milliards neuf cents quatre-vingts-dix-neuf millions neuf cents quatre-vingts-dix-neuf mille neuf cents quatre-vingts-dix-neuf");
    }



}
