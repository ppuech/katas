package fr.ssg.kata;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    @Test
    public void fizzbuzz_devrait_retourner_1_pour_1(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(1);
        Assertions.assertEquals("1", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Fizz_pour_3(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(3);
        Assertions.assertEquals("Fizz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Fizz_pour_6(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(6);
        Assertions.assertEquals("Fizz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Fizz_pour_moins3 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(-3);
        Assertions.assertEquals("Fizz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Fuzz_pour_5 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(5);
        Assertions.assertEquals("Buzz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Fuzz_pour_10 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(10);
        Assertions.assertEquals("Buzz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_FizzBuzz_pour_15 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(15);
        Assertions.assertEquals("FizzBuzz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_FizzBuzz_pour_moins30 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(-30);
        Assertions.assertEquals("FizzBuzz", resutlat);
    }

    @Test
    public void fizzbuzz_devrait_retourner_Tazz_pour_7 (){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String resutlat = fizzBuzz.verifierValeur(7);
        Assertions.assertEquals("Tazz", resutlat);
    }
    
    @Test
    public void vérificationFinale(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        for (int i = 0; i <= 15; i++) {
            System.out.print(fizzBuzz.verifierValeur(i) + " ");
        }

        String a = "1";
        Assertions.assertEquals("1", a);

    }
}
