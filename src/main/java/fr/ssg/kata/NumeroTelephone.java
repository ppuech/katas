package fr.ssg.kata;

public class NumeroTelephone {

    /**
     * Version Française
     * @param input les chiffres à formater
     * @return un numéro de téléphone
     */
    public String formaterNumeroTelephone(int[] input) {
        if (input.length != 10) {
            return "Format errone";
        }
        StringBuilder formated = new StringBuilder();
        for (int i = 0; i < input.length; i++) {
            if (i%2 == 0 && i != 0) {
                formated.append(" ");
            }
            formated.append(input[i]);
        }
        return formated.toString();
    }

    /**
     * Version optimisée
     */
    public String formaterNumeroTelephoneOptimise(int[] numbers) {
        return String.format("%d%d %d%d %d%d %d%d %d%d",numbers[0],numbers[1],numbers[2],numbers[3],numbers[4],numbers[5],numbers[6],numbers[7],numbers[8],numbers[9]);
    }

    /**
     * US version
     * @param numbers the 10 numbers to format
     * @return a phone number
     */
    public String createPhoneNumber(int[] numbers) {
        if (numbers.length != 10) {
            return "Bad format";
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numbers.length ; i++) {
            if( i == 0) {
                stringBuilder.append("(");
            }
            stringBuilder.append(numbers[i]);
            if( i == 2) {
                stringBuilder.append(") ");
            }
            if (i == 5) {
                stringBuilder.append("-");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Optimized version
     */
    public String createPhoneNumberOptimized(int[] numbers) {
        return String.format("(%d%d%d) %d%d%d-%d%d%d%d",numbers[0],numbers[1],numbers[2],numbers[3],numbers[4],numbers[5],numbers[6],numbers[7],numbers[8],numbers[9]);
    }



}

