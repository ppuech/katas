package fr.ssg.kata;

import java.util.List;

/**
 * Classe de convertion de chiffres en lettres (version française)
 * Notes :
 *  * Il faut pouvoir dépasser les 999 999 999 999 dans la limite autorisée par les "Long".
 *  * Il est envisageable d'être plus récursif. Plutôt que d'appeler la méthode "inférieur" pour définir une unité, on pourrait rappeler la méthode "convertion"
 *  pour ne pas s'occuper de savoir sur quel ordre de grandeur on est.
 *  Attention tout de même à la valeur "zéro" qui ne peut être définie que pour une seule valeur -> ça implique d'avoir une méthode  longermédiaire
 *  entre "convertion" et "definir..." pour définir les palliers
 *  * Si étape dessus effectuée, on pourrait générifier les méthodes : "definirMillier", "definirMillion" et "definirMilliard"
 *  * Est-il possible de génériser les palliers ? une Map ? J'aime bien les méthodes "definir..." qui sont porteuse des règles françaises.
 */
public class ChiffreEnLettre {

    List<String> referentielUnite = List.of("", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf");
    List<String> referentielUniteSpecial = List.of("onze", "douze", "treize", "quatorze", "quinze", "seize");
    List<String> referentielDizaines = List.of("dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante", "quatre-vingts", "quatre-vingts");
    List<String> referentielPuissanceUnite = List.of("cent", "mille", "million", "milliard");

    public String convertion( long valeur) {

        if( valeur< 0) {
            return  "moins " + convertion(Math.abs(valeur));
        } else if (valeur == 0) {
          return "zero";
        } else if (valeur < 10) {
            return definirUnite(valeur, null);
        } else if (valeur < 100) {
            return definirDizaine(valeur);
        } else if (valeur < 1000 ) {
            return definirCentaine(valeur);
        } else if (valeur < 1000000) {
            return definirMillier(valeur);
        } else if (valeur < 1000000000) {
            return definirMillion(valeur);
        } else {
            return definirMilliard(valeur);
        }

    }

    private String definirUnite( long valeur, String dizaine) {
        if (dizaine == null) {
            return referentielUnite.get((int)valeur);
        } else {
             long unite = valeur%10;
            if (unite == 1 && valeur != 81) {
                return dizaine + " et " + referentielUnite.get((int)unite);
            } else {
                return dizaine + "-" + referentielUnite.get((int)unite);
            }

        }
    }

    private String definirDizaineSpecifique(String dizaine,  long valeur) {
         long unite = valeur%10;
         long indexUnite = unite-1;
        String dix = referentielDizaines.get(0);

        if (valeur/10 == 1) {
            dizaine = "";
        } else {
            dizaine += "-";
        }

        if (unite == 0) {
            return dizaine + dix;
        } else if (unite > 0 && unite <7 ) {
            if ( valeur == 71) {
                return dizaine.replaceAll("-", "") + " et " + referentielUniteSpecial.get((int)indexUnite);
            }
            return dizaine + referentielUniteSpecial.get((int)indexUnite);
        } else {
            return dizaine + dix + "-" + referentielUnite.get((int)unite);
        }
    }

    private String definirDizaine( long valeur) {
         long indexDizaine = (valeur/10)-1;
        if (indexDizaine == -1) {
            return definirUnite(valeur, null);
        } else {

            String dizaine = referentielDizaines.get((int)indexDizaine);
             long unite = valeur % 10;

            if (indexDizaine == 0 || indexDizaine == 6 || indexDizaine == 8) {
                return definirDizaineSpecifique(dizaine, valeur);
            } else if (unite == 0) {
                return dizaine;
            } else {
                return definirUnite(valeur, dizaine);
            }


        }
    }

    private String definirCentaine( long valeur) {
        String cent = referentielPuissanceUnite.get(0);
        if ( valeur < 100 ) {
            return definirDizaine(valeur);
        } else if (valeur == 100) {
            return cent;
        } else {
             long uniteCentaine = (valeur/100);
            String valeurDizaines = definirDizaine(valeur%100);
            if (uniteCentaine > 1) {
                return referentielUnite.get((int)uniteCentaine) + " " + cent + "s " + valeurDizaines;
            } else {
                return cent + " " + valeurDizaines;
            }
        }
    }

    private String definirMillier( long valeur) {
        String mille = referentielPuissanceUnite.get(1);
        if (valeur < 1000) {
            return definirCentaine(valeur);
        } else if (valeur == 1000) {
            return mille;
        } else {
             long uniteMillier = (valeur/1000);
            String valeurMillier = definirCentaine(uniteMillier);
            String valeurCentaine = definirCentaine(valeur%1000);

            if (uniteMillier == 1) {
                return mille + " " + valeurCentaine;
            } else {
                return (valeurMillier + " " + mille + " " + valeurCentaine).trim();
            }
        }
    }

    private String definirMillion( long valeur) {
        String million = referentielPuissanceUnite.get(2);
        if (valeur < 1000000) {
            return definirMillier(valeur);
        } else if (valeur == 1000000) {
            return "un " + million;
        } else {
             long uniteMillion = (valeur/1000000);
            String valeurMillion = definirMillier(uniteMillion);
            String valeurMillier = definirMillier(valeur%1000000);

            if (uniteMillion == 1) {
                return "un " + million + " " + valeurMillier;
            } else {
                return (valeurMillion + " " + million + "s " + valeurMillier).trim();
            }
        }
    }

    private String definirMilliard( long valeur) {
        String milliard = referentielPuissanceUnite.get(3);
        if (valeur < 1000000000) {
            return definirMillion(valeur);
        } else if (valeur == 1000000000) {
            return "un " + milliard;
        } else {
            long uniteMilliard = (valeur/1000000000);
            String valeurMilliard = definirMillier(uniteMilliard);
            String valeurMillions = definirMillion(valeur%1000000000);

            if (uniteMilliard == 1) {
                return "un " + milliard + " " + valeurMillions;
            } else {
                return (valeurMilliard + " " + milliard + "s " + valeurMillions).trim();
            }
        }
    }
}
