package fr.ssg.kata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DuplicateEncoder {

    public String encode(String value){
        if (value == "") return "";

        List<String> originalList = Arrays.asList(value.toLowerCase().split(""));
        return originalList.stream()
                .map(character -> getEncodedCharacter(originalList,character ))
                .collect(Collectors.joining());
    }

    /**
     * Converti un caractère en "(" s'il n'apparait qu'une seule fois dans le tableau
     * en ")" sinon
     * @param splitedString la valeur originale sous forme de liste
     * @param caractere le caractère à traiter
     * @return "(" ou ")"
     */
    private String getEncodedCharacter(List<String> splitedString, String caractere) {
        return (Collections.frequency(splitedString, caractere) == 1) ? "(" : ")";
    }
}
