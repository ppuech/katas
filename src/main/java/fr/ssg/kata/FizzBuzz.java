package fr.ssg.kata;

public class FizzBuzz {

    public String verifierValeur(int valeur) {

        String sortie = "";

        if (valeur % 3 == 0) {
            sortie+="Fizz";
        }
        if (valeur % 5 == 0) {
            sortie+="Buzz";
        }
        if (valeur % 7 == 0) {
            sortie+="Tazz";
        }

        if (sortie == "") {
            return String.valueOf(valeur);
        } else {
            return sortie;
        }


    }



}
