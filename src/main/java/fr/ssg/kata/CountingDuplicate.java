package fr.ssg.kata;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Attention complexité actuelle : O(n²)
 *
 * TODO On peut le réduire en passant par des substrings .. à creuser
 *
 */
public class CountingDuplicate {

    private CountingDuplicate(){}

    /**
     * Méthode comptant le nombre de caractères apparaissant plusieurs fois au sein d'une valeur d'entrée.
     * Cette méthode est insensible à la casse. aA -> 1 (une duplication)
     * @param value la valeur d'entrée à analyser
     * @return le nombre de caractères dupliqués dans l'entrant.
     */
    public static int count(String value) {

        List<String> characters = Arrays.asList(value.toLowerCase().split(""));

        return (int) characters.stream().distinct()
                .filter(character -> Collections.frequency(characters, character) > 1)
                .count();
    }
}
