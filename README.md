# KATAS

[![pipeline-status](https://gitlab.com/ppuech/katas/badges/master/pipeline.svg)](https://gitlab.com/-/ide/project/ppuech/katas/-/commits/master)

## Description

Série de petits katas réalisés en TDD
Language : Java
Test : Junit5

## Liste des Katas

 * ChiffreEnLettre
 * CountingDuplicate ( perf à revoir )
 * DuplicateEncoder
 * FizzBuzzTazz
 * NumeroTelephone ( +PhoneNumber )
ava